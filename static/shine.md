## 闪屏API

+ URL: /api/start/
+ Header: 无验证
+ Method: GET
+ Return Data
```
{
    "img": "http:xxxxxxxxx",
    "url": "xxxxxxxxxxxxxx",
    "update": timestamp
}
```
+ status code
    - 200 OK
    - 502 服务器端异常