## BannerAPI

+ URL: /api/banner/
+ Header: None
+ Method: GET
+ Return Data:
```
[
    {
      "img": "http://muxistatic.com",
      "url": "http://muxistudio.com"
      'update': 时间戳
    },
   {....}
]
```
+ status code
    - 200 OK
    - 502 服务器端异常
