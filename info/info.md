## 通知公告API

+ URL: /api/info/
+ Header: None
+ Method: GET
+ Return Data:
```
{
    "title": "xxxx"
    "content": "xxxx" 
    "date": "2016-06-19",
    "appendix_list": [] // 下载链接
}
```
+ status code
	- 200 ok
	- 502 服务器端异常