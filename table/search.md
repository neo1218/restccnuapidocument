## 课表查询API

+ URL: /api/table/?xnm=2015&xqm=3
+ API查询参数说明
    - xnm: xnm年-xxx年, 例如2015-2016学年那么xnm=2015
    - xqm: 第一学期 xqm=3; 第二学期 xqm=12; 第三学期 xqm=16;
+ Header: 信息门户登录header
+ method: GET
+ return data(json):
```
[
 {
    "id": "1", // 课程id
    "course": "xxxx", // 课程名称
    "teacher": "xxxx", // 老师名
    "weeks": "1, 2, 3,,,,,19", // 上课周
    "day": 1, // 上课日: 星期1~7
    "start": 1, // 每天课程开始时间(ex: start=1 表示早上第一节课开始)
    "during": "2",  // 课程持续时间(ex: during=2 表示课程持续2节课)
    "place": "9-21",  // 上课地点
    "remind": true/false # 课程是否设置为提醒。用户自定义课程可以设置提醒, 信息门户课程设为false。
    "color": 0或1或2或3, # 课程格子的颜色(随机生成)
 },
 {...} ,
]
```
+ status code:
    - 200 ok
    - 502: 服务器端异常