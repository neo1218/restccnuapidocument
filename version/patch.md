## 补丁包版本控制API

返回所有补丁包的相关信息

+ URL: /api/patch/
+ Method: GET
+ Header: None
+ Return Data:
```
[
    {
       "version": "v1",  //  版本号,
       "download": "http://xxx.com",  // 下载地址
       "update": "2016-0802",  // 更新时间
       "intro": "这是一次史无前例的更新", // 更新说明
       "size": "100M"  // 补丁包大小
    },
    {...},
]
```
+ status code
    - 200 OK
    - 502 服务器端异常


返回最新的app版本信息

+ URL: /api/patch/latest/
+ Method: GET
+ Header: None
+ Return Data:
```
{
   "version": "v1",  # 版本号,
   "download": "http://xxx.com",  # 下载地址
   "update": "2016-0802",  # 更新时间
   "intro": "这是一次史无前例的更新", # 更新说明
   "size": "100M"  # 补丁包大小
}
```
+ status code
    - 200 OK
    - 502 服务器端异常


提交一个新的补丁包版本

+ URL: /api/patch/
+ Method: POST
+ Header: 管理员登录header
+ Post Data:
```
{
   "version": "v1",  # 版本号,
   "download": "http://xxx.com",  # 下载地址
   "update": "2016-0802",  # 更新时间
   "intro": "这是一次史无前例的更新", # 更新说明
   "size": "100M"  # 补丁包大小
}
```


删除特定版本(version)的补丁包信息

+ URL: /api/patch/<str:version>/
+ Method: DELETE
+ Header: 管理员登录header
+ return data
```
{}
```
+ status code
    - 200 OK
    - 502 服务器端异常