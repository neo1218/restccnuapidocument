## app版本控制API

**返回所有版本(包括历史版本)的相关信息**

+ URL: /api/app/
+ Method: GET
+ Header: None
+ Return Data:
```
[
    {
        "name": "华师匣子",
        "version": "v1",  // 版本号,
        "download": "http://xxx.com",  // 下载地址
        "update": "2016-0802",  // 更新时间
        "intro": "这是一次史无前例的更新", // 更新说明
        "size": "100M"  // 更新包大小
    },
    {...},
]
```
+ status code
    + 200 OK
    + 502 服务器端异常


**返回最新的app版本信息**

+ URL: /api/app/latest/
+ Method: GET
+ Header: None
+ Return Data:
```
{
   "name": "华师匣子",
   "version": "v1",  // 版本号,
   "download": "http://xxx.com",  // 下载地址
   "update": "2016-0802",  // 更新时间
   "intro": "这是一次史无前例的更新", // 更新说明
   "size": "100M"  // 更新包大小
}
```
+ status code
    - 200 OK
    - 502 服务器端异常


**添加一个新的版本**

+ URL: /api/app/
+ Method: POST
+ Header: 管理员登录header
+ Post Data:
```
{
   "name": "华师匣子",
   "version": "v1",  # 版本号,
   "download": "http://xxx.com",  # 下载地址
   "update": "2016-0802",  # 更新时间
   "intro": "这是一次史无前例的更新", # 更新说明
   "size": "100M"  # 更新包大小
}
```
+ status code
    - 200 OK
    - 502 服务器端异常


**删除特定版本(version)的app信息**

+ URL: /api/app/<str:version>/
+ Method: DELETE
+ Header: 管理员登录header
+ return data:
```
{}
```
+ status code
    - 200 OK
    - 502 服务器端异常