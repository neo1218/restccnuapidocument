## 图书查询API

+ URL: /api/lib/search/?keyword=xxx&page=1
+ API查询参数说明:
    - keyword: 搜索词(作者、书名...)
    - page: 查询页. 搜索结果会进行分页, 每页20条记录, 默认page为第一页
+ Header: 无验证
+ Method: GET
+ return data(json)
```
{
  "meta": {
       "max": 2  // 最大页数
       "per_page": 20  // 平均每页
   }
   "results": [
       {
           "book": "xxxx",  // 书名
           "author": "xxxx", // 作者名
           "bid":  "xxxxxxxxxxx", // 图书索书号
           "intro": "xxxxxxx", // 图书介绍(部分图书没有介绍)
           "id": "xxx"  // 图书的id(标识每一本图书)
       },
       {....},
   ]
}
```
+ status code
    - 200 ok
    - 502 服务器异常