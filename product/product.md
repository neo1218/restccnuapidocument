## 木犀产品展示

**获取所有展示的木犀产品**

+ URL: /api/product/
+ Header: None
+ Method: GET
+ Return Data
```
{
   "_product": [
   		{
       		"name": "学而",
       		"icon": "icon-urlxxxx",
            "url": "https://xueer.muxixyz.com",
            "intro": "华师课程经验挖掘机",
   		},
    	{....}
    ],
   "upate": "时间戳"
}
```
+ status code
	+ 200 OK
	+ 502 服务器端异常

**添加一个木犀产品**

+ URL: /api/product/
+ Header: 管理员登录header
+ Method: PUT
+ POST Data
```
{
	"name": "名称",
	"icon": "学而icon",
	"url": "学而网址",
	"intro": "学而介绍"
}
```
+ Return Data
```
{}
```
+ status code
	+ 200 OK
	+ 502 服务器端异常

**删除一个木犀产品**

+ URL: /api/product/?name=学而
+ Header: 管理员登录header
+ Method: DELETE
+ Return Data
```
{}
```
+ status code
	+ 200 OK
	+ 502 服务器端异常