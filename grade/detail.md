## 平时成绩查询API

+ URL: /api/grade/detail/search/?xnm=2015&xqm=3&course=hacks web开发&jxb_id=1
+ API查询参数说明:
    + **xnm**: xnm年-xxx年, 例如2015-2016学年那么xnm=2015
    + **xqm**: 第一学期 xqm=3, 第二学期 xqm=12, 第三学期 xqm=16
    + **course**: 课程名称
    + **jxb_id**: 一个奇怪的id.. 总成绩查询API会返回
+ Header: 信息门户登录header
+ Method: GET
+ return data:
```
{
    "usual": "60", # 平时分
    "ending": "60",  # 期末分
}
```
+ status code
	- 200 OK
	- 502 服务器端异常