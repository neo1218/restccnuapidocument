## 部门信息API

+ URL: /api/apartment/
+ Header: None
+ Method: GET
+ Return Data:
```
[
	{
	  "apartment": "学生事务大厅",  // 部门名称
	  "phone": ["67867510",],   // 部门电话
	  "place": "文华公书林东侧二楼" // 部门地点
	}，
	{....}
]
```
+ status code
	- 200 OK
	- 502 服务器端异常