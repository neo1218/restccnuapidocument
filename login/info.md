## 信息门户登录API
对应应用首页的登录

+ URL: /api/info/login/
+ Header: 信息门户登录header
+ Method: GET
+ return data(json):
```
{}
```
+ status code:
	- 200 OK
	- 403 禁止访问, 用户验证出错
	- 502 服务器端错误