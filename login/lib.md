## 图书馆登录API
对应图书馆登录

+ URL: /api/lib/login/
+ Header: 图书馆登录header
+ Method: GET
+ return data(json):
```
{}
```
+ status code:
	- 200 OK
	- 403 禁止访问, 用户验证出错
	- 502 服务器端错误